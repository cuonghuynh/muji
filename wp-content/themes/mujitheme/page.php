<?php get_header(); ?>

  	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="content<?php if (is_page()) { echo ' page'; } else { echo 'single'; } ?>">
			<?php 
				$parent_id = get_post_ancestors(get_the_id());
				
				if ( has_post_thumbnail($parent_id[0]) ) {
					$image = getFeaturedImage($parent_id[0], 'full');
				} else {
					$image = getFeaturedImage(get_the_id(), 'full');

				}
			?>
			<div class="thumb-wrapper" style="background-image: url('<?php echo $image[0] ?>');"></div>
			<div class="content-wrapper">
				<?php the_content(); ?>
			</div>
			
		</div> <!-- /page -->

  	<?php endwhile; endif; ?>


<?php get_footer(); ?>
