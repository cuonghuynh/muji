<?php get_header(); ?>
<div class="content<?php if (is_page()) { echo ' page'; } else { echo ' single'; } ?>">
	<?php 
		$image = getFeaturedImage( 26 , 'full');
	?>
	<div class="thumb-wrapper" style="background-image: url('<?php echo $image[0] ?>');"></div>
		<div class="content-wrapper">
  <?php if (have_posts()) : ?>

	 <h1>Search Results</h1>

	 <?php while (have_posts()) : the_post(); ?>

			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			  <header>
				 <a href="<?php echo get_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
				 <?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
			  </header>
			  <div class="entry">
				 <?php the_excerpt(); ?>
			  </div>
			</article>

	 <?php endwhile; ?>

	 <?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

  <?php else : ?>

	 <h1>No posts found.</h1>

  <?php endif; ?>

  	</div>	
</div> <!-- /page -->

<?php get_footer(); ?>
