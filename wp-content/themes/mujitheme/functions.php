<?php
  // Add RSS links to <head> section
  automatic_feed_links();

  // UNCOMMENT THE FOLLOWING IF YOU NEED JQUERY LOADED:
  /*
  if ( !is_admin() ) {
	  wp_deregister_script('jquery');
	  wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"), false);
	  wp_enqueue_script('jquery');
  }
  */

  // Clean up the <head>
  function removeHeadLinks() {
	 remove_action('wp_head', 'rsd_link');
	 remove_action('wp_head', 'wlwmanifest_link');
  }

  add_action('init', 'removeHeadLinks');
  remove_action('wp_head', 'wp_generator');

  if (function_exists('register_sidebar')) {
	 register_sidebar(array(
		'name' => 'About Us Gallery Widget',
		'id'   => 'child-page-widgets',
		'description'   => 'The widget will be displayed in About Us page.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1>',
		'after_title'   => '</h1>'
	 ));
  }

  function blankhtml5_comment($comment, $args, $depth) {
	 $GLOBALS['comment'] = $comment; ?>
	 <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<article id="comment-<?php comment_ID(); ?>">
		  <header class="comment-author vcard">
			 <?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?>

			 <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
			 <?php if ($comment->comment_approved == '0') : ?>
				<em><?php _e('Your comment is awaiting moderation.') ?></em><br />
			 <?php endif; ?>

			 <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>
		  </header>

		  <?php comment_text() ?>

		  <div class="reply">
			 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		  </div>
		</article>
  <?php
  }

  	/**
  	 * Define new image upload size
  	 */
  	add_action( 'after_setup_theme', 'my_upload_size' );
	function my_upload_size() {
	  	add_image_size('about-page-thumb', 490, 325, true ); // 300 pixels wide (and unlimited height)
	}

	add_action( 'init' , 'my_add_excerpt_to_pages' );
  	function my_add_excerpt_to_pages()
  	{
  		add_post_type_support( 'page' , 'excerpt' );
  	}

	add_filter( 'image_size_names_choose', 'my_custom_sizes' );
	/**
	 * Add new image size option to the list of selectable sizes in Media Library
	 * @param  [type] $sizes [description]
	 * @return [type]        [description]
	 */
	function my_custom_sizes( $sizes ) {
	    return array_merge( $sizes, array(
	        'about-page-thumb' => __('About-Us Image (490x325)'),
	    ) );
	}

	if (function_exists('add_theme_support')) {
  		add_theme_support( 'post-thumbnails', array('post', 'page') );
  	}

  	if(function_exists('register_nav_menus')){
	  	register_nav_menus (array(
	  		'main_menu' => 'Main Navigation Menu',
	  		));
  	}

  	//include childepage-gallery widget
  	include( TEMPLATEPATH . '/inc/childpage-gallery.php' );

  	//include display breadcrumb function
  	include( TEMPLATEPATH . '/inc/breadcrumb.php' );

  	//include generate opengraph function
  	include( TEMPLATEPATH . '/inc/opengraph.php' );

  	//include Share Class
  	include( TEMPLATEPATH . '/inc/share.php' );

  	/**
  	 * Get Thumbnail Image of Page, Post
  	 * @param  integer $id        	The post/page ID
  	 * @param  string $thumbSize 	Uploaded Image size
  	 * @return array             
  	 */
	function getFeaturedImage($id, $thumbSize = 'full')
	{
		$thumbID = get_post_thumbnail_id( $id );
		$thumbUrl = wp_get_attachment_image_src($thumbID, $thumbSize, true);
		return $thumbUrl;
	}
?>