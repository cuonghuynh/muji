<?php 

function the_breadcrumb()
{
	global $post;		//current query post

	if (!is_home()) {
		echo '<div class="top-breadcrumb">';
		echo '<ul>';
		if ( is_category() || is_single() ) {
			$cats = get_the_category();
			echo '<li><a href="' . get_category_link( $cats[0]->cat_ID ) . '">' . $cats[0]->name . '</a></li>';
			if ( is_single() ) {
				echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
			}
		} elseif ( is_page() ) {
			if ($post->post_parent) {
				$anc = get_post_ancestors( $post->ID );
				foreach ($anc as $key => $ancestor) {
					echo '<li><a href="' . get_permalink($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
				}
			}
			echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
		}
		echo '</ul>';
		echo '</div>';
	}
	
}

?>