<?php
/**
 * ChildPage Gallery Widget Class
 */
class childpage_gallery_widget extends WP_Widget {
 
 
		/** constructor -- name this the same as the class above */
		function childpage_gallery_widget() {
				parent::WP_Widget(false, $name = 'ChildPage Gallery Widget');	
		}
 
		/** @see WP_Widget::widget -- do not rename this */
		/**
		 * HTML display in template
		 * @param  array $args     Store $before_widget, $after_widget, $before_title, $after_title HTML
		 * @param  array $instance Store fields
		 * @return avoid
		 */
		function widget($args, $instance) {	
				extract( $args );		
				?>
							<?php echo $before_widget; ?>
							
									<div class="about-us-gallery">
										<div class="row">
							<?php 
								$current = get_the_id();
								$args = array(
                                	'child_of'  => $current,    // current page id
                                	'sort_column' => 'post_date',
                                	'sort_order' => 'asc',
                                	);
                            	$child_page_list = get_pages( $args );

							?>
								<?php foreach ($child_page_list as $key => $item): ?>

									<?php if ($item->post_parent == $current): 
										$image = getFeaturedImage($item->ID, 'about-page-thumb');
									?>
											<div class="col-sm-6 item">
												<a href="<?php echo $item->guid; ?>">
													<img class="thumb" src="<?php echo $image[0]; ?>" alt="">
													<div class="filter-mask"></div>
													<span class="title"><?php echo $item->post_title; ?></span>
												</a>
											</div>
									<?php endif ?>

								<?php endforeach ?>	
		
										</div>
									</div>
							<?php echo $after_widget; ?>
				<?php
		}
 
		/** @see WP_Widget::update -- do not rename this */
		/**
		 * Update item value in database
		 * @param  array $new_instance Store new value
		 * @param  array $old_instance Store old value
		 * @return array
		 */
		function update($new_instance, $old_instance) {		
		$instance = $old_instance;
				return $instance;
		}
 
		/** @see WP_Widget::form -- do not rename this */
		/**
		 * This Form display in Admin page
		 * @param  array $instance
		 * @return avoid
		 */
		function form($instance) {	

		}
 
 
} // end class example_widget
add_action('widgets_init', create_function('', 'return register_widget("childpage_gallery_widget");'));
?>
