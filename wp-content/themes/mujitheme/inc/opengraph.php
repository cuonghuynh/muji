<?php 

function the_open_graph()
{
	global $post;

	$title = '';
	$type = 'article';
	$url = '';
	$image = '';
	$description = '';

	$about_us_ID = 26;

	if (!is_home()) {

		if ( is_page() || is_single() ) {

			$title = get_the_title();
			$url = get_permalink();
			if ( has_post_thumbnail() ) {
				$image = getFeaturedImage( get_the_ID() , 'full');
			} else {
				$image = getFeaturedImage( $about_us_ID , 'full');
			}
			
			$description = get_the_excerpt();

		} else {
			
			$title = get_the_title($about_us_ID);
			$url = get_permalink($about_us_ID);
			$image = getFeaturedImage( $about_us_ID , 'full');
			$description = get_the_excerpt( $about_us_ID );

		}

	}

	echo '<meta property="og:title" content="' . $title . '" />';
	echo '<meta property="og:type" content="' . $type . '" />';
	echo '<meta property="og:url" content="' . $url . '" />';
	echo '<meta property="og:image" content="' . $image[0] . '" />';
	echo '<meta property="og:description" content="' . $description . '" />';

}

?>