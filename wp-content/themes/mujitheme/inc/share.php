<?php 

class Share
{
	private $link;
	private $title;

	/**
	 * Load value
	 * @param string $link  	Article URL
	 * @param string $title 	Article title
	 */
	public function Share($link, $title = '')
	{
		$this->link = urlencode($link);
		$this->title = urlencode($title);

		return $this;
	}

	public function Facebook()
	{
		return 'https://www.facebook.com/sharer/sharer.php?u='.$this->link.(($this->title) ? '&title='.$this->title : '');
	}

	public function Twitter()
	{
		return 'https://twitter.com/intent/tweet?url='.$this->link.(($this->title) ? '&text='.$this->title : '');
	}

	public function GooglePlus()
	{
		return 'https://plus.google.com/share?url='.$this->link;
	}

}
?>