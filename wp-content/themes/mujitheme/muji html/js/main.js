$(document).ready(function() {

	showMenuPopup();

	hoverShareButton();

	setEffectToGalleryItem();

	initMasonry();

});

function showMenuPopup()
{
	$('.menu-button').click(function(event) {
		if ( $('#modal-popup').hasClass('show') ) {
			$('#modal-popup').removeClass('fadeInDown').addClass('fadeOutUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(event) {
					$(this).removeClass('show');
					$('body').removeClass('hide-scrollbar');
			});;
		} else {
			$('#modal-popup').removeClass('fadeOutUp').addClass('animated fadeInDown show');
			$('body').addClass('hide-scrollbar');
		}
		
	});
}

function hoverShareButton()
{
	$('.share-button').hover(function() {
		$(this).addClass('shadow');
	}, function() {
		$(this).removeClass('shadow');
	});
}

function setEffectToGalleryItem()
{
	$('.about-us-gallery .item').hover(function() {

		//$(this).find('.title').addClass('animated flash');
		$(this).find('.filter-mask').addClass('animated fadeIn show');

	}, function() {

		//$(this).find('.title').removeClass('animated flash');
		$(this).find('.filter-mask').removeClass('animated fadeIn show');

	});
}

function initMasonry()
{
	var wall = $('#wall');
	wall.masonry({
		columnWidth: 325,
		itemSelector: '.people',
	});

	$('#wall .people').hover(function() {
		$(this).find('.mask-wrapper').addClass('animated fadeIn show');
	}, function() {
		$(this).find('.mask-wrapper').removeClass('animated fadeIn show');
	});

}