<?php
/*
Template Name: Slider Page
*/

get_header(); ?>

	 	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<div class="content<?php if (is_page()) { echo ' page'; } else { echo 'single'; } ?>">
				<?php the_content(); ?>
			</div> <!-- /page -->

	 	<?php endwhile; ?>

	 	<?php else : ?>

			<h1>Not Found</h1>

	 	<?php endif; ?>

<?php get_footer(); ?>