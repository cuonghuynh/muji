<?php
/*
Template Name: People Page
*/
 get_header(); ?>

  	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="content<?php if (is_page()) { echo ' page'; } else { echo 'single'; } ?>">
			<?php 
				$parent_id = get_post_ancestors(get_the_id());
				
				if ( has_post_thumbnail($parent_id[0]) ) {
					$image = getFeaturedImage($parent_id[0], 'full');
				} else {
					$image = getFeaturedImage(get_the_id(), 'full');
				}
			?>
			<div class="thumb-wrapper" style="background-image: url('<?php echo $image[0] ?>');"></div>
			<div class="content-wrapper">
				<?php the_content(); ?>
				<div id="wall">
				<?php
					$people = get_field('people');
				?>
				<?php foreach ($people as $key => $item): ?>
					<div class="people <?php echo ($item['gallery_mode'] == 'Large' ) ? 'lg' : ''; ?>">
						<div class="avatar" style="background-image: url('<?php echo $item['avatar']; ?>');"></div>
						<div class="mask-wrapper">
							<div class="mask-content">
								<div class="description">
									<p><span class="job"><?php echo $item['job']; ?></span>
									<?php echo $item['description']; ?></p>
								</div>
							</div>
						</div>
						<span class="name"><?php echo $item['name']; ?></span>
					</div>
				<?php endforeach ?>
				</div>
			</div>
		</div> <!-- /page -->

  	<?php endwhile; endif; ?>


<?php get_footer(); ?>
