<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
  	<meta charset="<?php bloginfo('charset'); ?>" />

  	<?php if (is_search()) { ?>
	 	<meta name="robots" content="noindex, nofollow" />
  	<?php } ?>
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="description" content="">
  	<meta name="keywords" content="">
  	<meta name="author" content="">
  	<?php the_open_graph(); ?>
  	<title>
	<?php
		if (function_exists('is_tag') && is_tag()) {
			single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		elseif (is_archive()) {
			wp_title(''); echo ' Archive - '; }
		elseif (is_search()) {
			echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		elseif (!(is_404()) && (is_single()) || (is_page())) {
			echo the_title(); echo ' - '; }
		elseif (is_404()) {
			echo 'Not Found - '; }
		if (is_home()) {
			bloginfo('name'); echo ' - '; bloginfo('description'); }
		else {
			 bloginfo('name'); }
		if ($paged>1) {
			echo ' - page '. $paged; }
	?>
  	</title>

  	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  	<link href="<?php echo get_bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
  	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" />
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/animate.css" />

  	<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/modernizr-1.5.min.js"></script>

  	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

  	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  	<div id="site-wrapper">
  		<div id="search-modal" class="container">
  			<div class="search-wrapper">
  				<div class="close-button">
  					<i class="fa fa-close"></i>
  				</div>
  				<div class="search-form">
  					<?php get_search_form(); ?>
  				</div>
  			</div>
  		</div>
		<div id="content" class="container">
			<div class="header">
				<div class="row">
					<div class="col-sm-6 col-sm-push-6">
						<div class="logo-wrapper">
							<?php 

								if ( function_exists( 'ot_get_option' ) ) {
									$logo = ot_get_option( 'logo' );
								}

							?>
							<a href="<?php echo home_url(); ?>"><img src="<?php echo $logo; ?>" alt=""></a>
						</div> <!-- /logo-wrapper -->
					</div>
					<div class="col-sm-6 col-sm-pull-6">
						<div class="top-wrapper">
							<div class="menu-button">
								<i class="fa fa-bars"></i>
							</div>
							<?php the_breadcrumb(); ?>
						</div> <!-- /top-wrapper -->
					</div>
				</div>
			</div> <!-- /header -->
			<div id="modal-popup">
				<div class="container">
					<div class="modal-wrapper">
						<div class="main-menu">
							<ul>
								<?php 

								$menu_name      = 'main_menu';
								$locations      = get_nav_menu_locations();
								$menu           = wp_get_nav_menu_object( $locations[ $menu_name ] );
								$menu_items     = wp_get_nav_menu_items( $menu->term_id );	//array
								if ( !empty($menu_items) ) {
									foreach ($menu_items as $key => $item) {
										echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
									}
								}
								?>
								<li><a href="#" id="search" >Search</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div> <!-- /modal -->