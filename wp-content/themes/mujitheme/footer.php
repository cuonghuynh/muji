    		<div class="footer">
    			<?php if (!is_home()): ?>
    				<div class="share-group">
						<div class="title">Share this case study</div>
						<div class="share-button-list">
							<ul>
								<?php 
									if ( function_exists( 'ot_get_option' ) ) {
										$facebook = ot_get_option( 'facebook' );
										$twitter = ot_get_option( 'twitter' );
										$gplus = ot_get_option( 'google_plus' );
										
										$share = new Share(get_permalink(), '');

									}

								?>
								<?php if ($facebook == 'on'): ?>
									<li>
										<a href="<?php echo $share->Facebook(); ?>" class="share-button"><i class="fa fa-facebook"></i></a>
									</li>
								<?php endif ?>
								<?php if ($twitter == 'on'): ?>
									<li>
										<a href="<?php echo $share->Twitter(); ?>" class="share-button"><i class="fa fa-twitter"></i></a>
									</li>
								<?php endif ?>
								<?php if ($gplus == 'on'): ?>
									<li>
										<a href="<?php echo $share->GooglePlus(); ?>" class="share-button"><i class="fa fa-google-plus"></i></a>
									</li>
								<?php endif ?>
							</ul>
						</div>
					</div>
    			<?php endif ?>
			</div>	<!-- /footer -->
		</div> <!-- /container -->	
	</div> <!-- /site-wrapper -->

  <?php wp_footer(); ?>

  	<!-- Don't forget analytics -->
  	<!-- Placed at the end of the document so the pages load faster -->
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.2.2/masonry.pkgd.min.js"></script>
  	<script src="<?php echo get_bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo get_bloginfo('template_url'); ?>/js/main.js"></script>

</body>
</html>